Libraries used to build this app - 
1) Backbone
2) jQuery
3) PubSub 
4) Underscore
5) Bootstrap

API endpoints expected from backend - 

-> User APIs
	GET - /users
	POST - /users
	PUT - /user/{id}
	DELETE - /user/{id}

-> Group APIs
	GET - /groups
	POST - /groups
	PUT - /group/{id}
	DELETE - /group/{id}



Further improvements that can be done - 
1) Use localstorage to store data offline.
2) Usage of bootstrap modals when creating new user/group.
3) Usage of subtle animations to give a rich user exp.
4) Implementation of unit tests.