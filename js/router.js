"use strict";
window.APP = window.APP || {};
APP.Router = Backbone.Router.extend({
  routes: {
    "user/new": "userCreate",
    "users/index": "userIndex",
    "user/:id/edit": "userEdit",
    "user/:id/view": "userShow",

    "groups/index": "groupIndex",
    "group/new": "createGroup",
    "group/:id/edit": "editGroup",
    "group/:id/view": "showGroup",
  },

  initialize: function (options) {
    this.users = options.users;
    this.groups = options.groups;
    this.userIndex();
  },

  userCreate: function () {
    this.currentView = new APP.userNewView({
      users: this.users, 
      user: new APP.userModel(),
      groups: this.groups
    });
    
    this.addToDom(this.currentView);
    this.addActiveClass("user");
  },

  userEdit: function (id) {
    var user = this.users.get(id);
    this.currentView = new APP.userEditView({
      user: user,
      groups: this.groups
    });
    this.addToDom(this.currentView);
    this.addActiveClass("user");
  },

  userShow: function (id) {
    var user = this.users.get(id);
    this.currentView = new APP.userShowView({
      user: user
    });
    this.addToDom(this.currentView);
    this.addActiveClass("user");
  },

  userIndex: function () {
    this.currentView = new APP.userIndexView({
      users: this.users
    });

    this.searchView = new APP.userSearchView();
    $('.search-box').html(this.searchView.render().el);

    this.addToDom(this.currentView);

    this.addActiveClass("user");

    $('.user-actions').show();
    $('.group-actions').hide();
    
  },

  createGroup: function() {

    this.currentView = new APP.groupNewView({
      group: new APP.groupModel(),
      groups: this.groups
    });
    
    this.addToDom(this.currentView);

  },

  groupIndex: function () {
    this.currentView = new APP.groupIndexView({
      groups: this.groups,
      users: this.users
    });

    this.searchView = new APP.groupSearchView();
    $('.search-box').html(this.searchView.render().el);

    this.addToDom(this.currentView);
    this.addActiveClass("group");

    $('.user-actions').hide();
    $('.group-actions').show();
  },

  editGroup: function(id) {
    var group = this.groups.get(id);
    this.currentView = new APP.groupEditView({
      group: group,
      groups: this.groups
    });
    this.addToDom(this.currentView);
    this.addActiveClass("group");
  },

  showGroup: function(id) {
    var group = this.groups.get(id);
    this.currentView = new APP.groupShowView({
      group: group,
      users: this.users
    });
    this.addToDom(this.currentView);
    this.addActiveClass("group");
  },


  addActiveClass: function(route) {
    if(route === "user") {
      $('.user-nav').addClass('active');
      $('.group-nav').removeClass('active');
    } else if(route === "group"){
      $('.group-nav').addClass('active');
      $('.user-nav').removeClass('active');
    }
  },

  addToDom: function(view) {
    $('#primary-content').html(view.render().el);
  }
});
