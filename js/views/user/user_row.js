"use strict";
APP.userRowView = Backbone.View.extend({
  tagName: "tr",
  events: {
    "click a.delete": "destroy"
  },

  initialize: function (options) {
    this.user  = options.user;
    this.users = options.users;
  },

  render: function () {
    this.$el.html(_.template($('#userRowTemplate').html(), this.user.toJSON()));
    return this;
  },

  destroy: function (event) {
    var self = this;

    event.preventDefault();
    event.stopPropagation();
    
    this.users.remove(this.user);
    this.$el.fadeOut(function() {
      this.$el.remove();
    });
  }
});
