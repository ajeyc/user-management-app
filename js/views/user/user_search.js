
"use strict";
APP.userSearchView = Backbone.View.extend({
  events: {
    "keyup #searchUser": "searchHandler"
  },

  searchHandler: function(e) {
    var text = $(e.currentTarget).val();
    publish("user.search", [text]);
  },

  render: function() {
    this.$el.html($('#searchTemplate').html());
    return this;
  }
});

