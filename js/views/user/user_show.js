
"use strict";
APP.userShowView = Backbone.View.extend({
  initialize: function (options) {
    this.user = options.user;
  },

  render: function () {
    this.$el.html(_.template($('#showTemplate').html(), this.user.toJSON()));
    return this;
  }
});

