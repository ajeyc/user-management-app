
"use strict";
APP.userIndexView = Backbone.View.extend({

  events: {

  },

  initialize: function (options) {
    var self = this;
    this.users = options.users;
    this.users.bind('reset', this.addAll, this);
    
    subscribe('user.search', function(text){
      var filtered = self.users.filter(function(model) {
          return _.any(model.attributes, function(val, attr) {
              return ~val.toString().toLowerCase().indexOf(text.toLowerCase());
          });;
      });

      self.renderSearchResults(filtered);
      
      // if(text === "") {
      //   self.render();
      // } else {
      //   var filtered = this.users.where({name: text});
      //   self.renderSearchResults(filtered);  
      // }
      
    });

    //this.lazySearch = _.debounce(this.searching, 200);
  },

  renderSearchResults: function(filtered) {
    this.$el.html($('#userIndexTemplate').html());
    _.each(filtered, $.proxy(this, 'addOne'));
  },

  render: function () {
    this.$el.html($('#userIndexTemplate').html());
    this.addAll();
    return this;
  },

  addAll: function () {
    this.$el.find('tbody').children().remove();
    _.each(this.users.models, $.proxy(this, 'addOne'));
  },

  addOne: function (user) {
    var view = new APP.userRowView({
      users: this.users, 
      user: user
    });
    this.$el.find("tbody").append(view.render().el);
  }
});

