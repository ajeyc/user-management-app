"use strict";
APP.userEditView = Backbone.View.extend({
  events: {
    "click button.save": "save"
  },

  initialize: function (options) {
    this.user  = options.user;
    this.groups = options.groups;
  },

  save: function (event) {
    event.stopPropagation();
    event.preventDefault();

    this.user.set({
      name: this.$el.find('input[name=name]').val(),
      group: $('#groupList').val()
    });

    // redirect to user index
    Backbone.history.navigate('users/index', { trigger: true });
  },

  render: function () {
    this.$el.html(_.template($('#userFormTemplate').html(), this.user.toJSON()));
    this.updateSelect();
    return this;
  },

  // Update select box with latest groups and then add selected group
  updateSelect: function() {
    var self = this;
    window.setTimeout(function() {
      var selectBox = $('#groupList');
      selectBox.empty();
      _.each(this.groups.models, function(group) {
          selectBox.append($("<option></option>").attr("value", group.get('name')).text(group.get('name')));
          if(self.user.get('group') === group.get('name')) {
            selectBox.val(group.get('name'));
          }
      });
    }, 0);
  }
});
