
"use strict";
APP.userNewView = Backbone.View.extend({

  events: {
    "click button.save": "save"
  },

  initialize: function (options) {
    this.user  = options.user;
    this.users = options.users;
    this.groups = options.groups;
    this.user.bind('invalid', this.showErrors, this);
    this.updateSelectList();
  },

  updateSelectList: function() {
    window.setTimeout(function() {
      var selectBox = $('#groupList');
      selectBox.empty();
      _.each(this.groups.models, function(group) {
          selectBox.append($("<option></option>").attr("value", group.get('name')).text(group.get('name')));
      });
    }, 0);
  },

  showErrors: function (user, errors) {
    this.$el.find('.error').removeClass('error');
    this.$el.find('.alert').html(_.values(errors).join('<br>')).show();
    // highlight the fields with errors
    _.each(_.keys(errors), _.bind(function (key) {
      this.$el.find('*[name=' + key + ']').parent().addClass('error');
    }, this));
  },

  save: function (event) {
    event.stopPropagation();
    event.preventDefault();

    this.user.set({
      name: this.$el.find('input[name=name]').val(),
      group: $('#groupList').val(),
      id: Math.floor(Math.random() * 100) + 1
    });
    if (this.user.isValid()){
      this.users.add(this.user);
      window.location.hash = "users/index";
    }
  },

  render: function () {
    this.$el.html(_.template($('#userFormTemplate').html(), this.user.toJSON()));
    return this;
  }
});
