
"use strict";
APP.groupSearchView = Backbone.View.extend({
  events: {
    "keyup #searchGroup": "searchHandler"
  },

  searchHandler: function(e) {
    var text = $(e.currentTarget).val();
    publish("group.search", [text]);
  },

  render: function() {
    this.$el.html($('#searchTemplate').html());
    return this;
  }
});

