
"use strict";
APP.groupShowView = Backbone.View.extend({
  events: {
    'click .delete-user': 'deleteUser'
  },

  initialize: function (options) {
    this.group = options.group;
    this.users = options.users;
    this.addUsersInGroup();
  },

  addUsersInGroup: function() {
  	var users = [];
  	var self = this;
  	_.each(this.users.models, function(user) {
  		if(user.get('group') === self.group.get('name')) {
  			users.push({
          id: user.get('id'),
          name: user.get('name')
        });
  		}
  	});
  	this.group.set('users', users);
  },

  // Delete the selected user from the user collection.
  deleteUser: function(e) {
    var userId = Number($(e.currentTarget).attr('data-userId'));
    var selectedList = $(e.currentTarget).parent();
    var userModel = this.users.get(userId);
    userModel.destroy();
    this.users.remove(userModel);
    selectedList.fadeOut(function(){
      selectedList.remove();
    });
    this.render();
  },

  render: function () {
    this.$el.html(_.template($('#groupShowTemplate').html(), this.group.toJSON()));
    return this;
  }
});

