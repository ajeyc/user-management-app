
"use strict";
APP.groupEditView = Backbone.View.extend({
  events: {
    "click button.save": "save"
  },

  initialize: function (options) {
    this.group  = options.group;
    this.groups = options.groups;
  },

  save: function (event) {
    event.stopPropagation();
    event.preventDefault();

    this.group.set({
      name: this.$el.find('input[name=name]').val(),
      group: $('#groupList').val()
    });

    Backbone.history.navigate('groups/index', { trigger: true });
  },

  render: function () {
    this.$el.html(_.template($('#groupFormTemplate').html(), this.group.toJSON()));
    return this;
  }
});
