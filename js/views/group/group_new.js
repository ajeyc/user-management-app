
"use strict";
APP.groupNewView = Backbone.View.extend({

  events: {
    "click button.save": "save"
  },

  initialize: function (options) {
    this.group  = options.group;
    this.groups = options.groups;
    this.group.bind('invalid', this.showErrors, this);
  },

  showErrors: function (user, errors) {
    this.$el.find('.error').removeClass('error');
    this.$el.find('.alert').html(_.values(errors).join('<br>')).show();
    // highlight the fields with errors
    _.each(_.keys(errors), _.bind(function (key) {
      this.$el.find('*[name=' + key + ']').parent().addClass('error');
    }, this));
  },

  save: function (event) {
    event.stopPropagation();
    event.preventDefault();

    this.group.set({
      name: this.$el.find('input[name=name]').val(),
      id: Math.floor(Math.random() * 100) + 1
    });
    if (this.group.isValid()){
      this.groups.add(this.group);
      Backbone.history.navigate('groups/index', { trigger: true });
    }
  },

  render: function () {
    this.$el.html(_.template($('#groupFormTemplate').html(), this.group.toJSON()));
    return this;
  }
});
