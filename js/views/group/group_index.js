
"use strict";
APP.groupIndexView = Backbone.View.extend({
  events: {

  },

  initialize: function (options) {
    var self = this;
    this.groups = options.groups;
    this.groups.bind('reset', this.addAll, this);
    this.users = options.users;
    
    subscribe('group.search', function(text){
      var filtered = self.groups.filter(function(model) {
          return _.any(model.attributes, function(val, attr) {
              return ~val.toString().toLowerCase().indexOf(text.toLowerCase());
          });;
      });

      self.renderSearchResults(filtered);
      
    });

    $('#group-alert').hide();
  },

  renderSearchResults: function(filtered) {
    this.$el.html($('#groupIndexTemplate').html());
    _.each(filtered, $.proxy(this, 'addOne'));
  },

  render: function () {
    this.$el.html($('#groupIndexTemplate').html());
    this.addAll();
    return this;
  },

  addAll: function () {
    this.$el.find('tbody').children().remove();
    _.each(this.groups.models, $.proxy(this, 'addOne'));
  },

  addOne: function (group) {
    var view = new APP.groupRowView({
      groups: this.groups, 
      group: group,
      users: this.users
    });
    this.$el.find("tbody").append(view.render().el);
  }
});

