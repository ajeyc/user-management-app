"use strict";
APP.groupRowView = Backbone.View.extend({
  tagName: "tr",
  events: {
    "click a.delete": "destroy"
  },

  initialize: function (options) {
    this.group  = options.group;
    this.groups = options.groups;
    this.users = options.users;
  },

  render: function () {
    this.$el.html(_.template($('#groupRowTemplate').html(), this.group.toJSON()));
    return this;
  },

  destroy: function (event) {
    event.preventDefault();
    event.stopPropagation();
    var self = this;

    if(this.hasUsers()) {
      $('#group-alert').show().delay(4000).fadeOut();
    } else {
      this.groups.remove(this.group);
      this.$el.fadeOut(function() {
        this.$el.remove();
      });
    }
  },

  hasUsers: function() {
    var self = this;
    var hasUser = false;
    this.users.filter(function(user) {
      if(user.get('group') === self.group.get('name')) {
        hasUser = true;
      }
    });
    return hasUser;
  }
});
