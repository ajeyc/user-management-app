
"use strict";
APP.userModel = Backbone.Model.extend({
  defaults: {
    name: "",
    group: ""
  },

  validate: function (attrs) {
    var errors = {};
    if (!attrs.name) errors.name = "User must have a name";
    if (!attrs.group) errors.group = "User must belong to a group";

    if (!_.isEmpty(errors)) {
      return errors;
    }
  }
});

APP.userCollection = Backbone.Collection.extend({

  model: APP.userModel
});
