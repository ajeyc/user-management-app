
"use strict";
APP.groupModel = Backbone.Model.extend({
  defaults: {
    name: "",
    users: []
  },

  validate: function (attrs) {
    var errors = {};
    if (!attrs.name) errors.name = "Group must have a name";

    if (!_.isEmpty(errors)) {
      return errors;
    }
  }
});

APP.groupCollection = Backbone.Collection.extend({
  // Reference to this collection's model.
  model: APP.groupModel
});
