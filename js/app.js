var users = new APP.userCollection();
var groups = new APP.groupCollection();

users.reset([
  {
    "name": "spiderman",
    "id": "1",
    "group": "HR"
  },
  {
    "name": "batman",
    "id": "2",
    "group": "Tech"
  },
  {
    "name": "superman",
    "id": "3",
    "group": "Sales"
  }
]);

groups.reset([
  {
    "name": "HR",
    "id": "11"
  },
  {
    "name": "Tech",
    "id": "2"
  },
  {
    "name": "Sales",
    "id": "3"
  }
]);

var router = new APP.Router({
  users: users,
  groups: groups
});

Backbone.history.start();